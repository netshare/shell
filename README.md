# netshare shell scripts

Conception pédagogique de scripts Bourne Shell pour UNIX.

L'objectif est d'écrire des scripts portables et fiables que chacun
peut utiliser et étudier dans une optique de découverte approfondie de
l'interface d'utilisation "native" des systèmes de type UNIX.

Beaucoup de techniques utilisées sont discutables sur le fait qu'elles
soient portables ou non et il est relativement complexe de tester le
bon fonctionnement sur de nombreuses plateformes. La plus répandue
pour le grand publique est GNU avec de nombreux outils
d'administration de Linux en supplément, nous tenterons d'être plus
générique afin de ne pas s'y limiter.

Nous utiliserons le principe de versions sémantiques pour chaque utilitaire.

## Avertissement

Nos utilitaires ne sont pas prêts pour une utilisation en production
et nous déclinons toute responsabilité pour d'éventuels dommages
causés par ces derniers.

## Outils

### lsfile

Lsfile permet de lister les types de fichiers par reconnaissance de
leur empreintes via l'utilitaire file. File n'est pas standardisé mais
il est présent sur de nombreuses plateformes et s'avère bien pratique.

Nous utiliserons find, xargs, sort, file, awk et de manière
optionnelle, column pour formater la sortie comme souhaité.

#### Usage

```
Usage: lsfile [OPTIONS]... [FILE]...
Generate a list of file types presents in given paths and their sum.

  -x	Don't descend directories on other filesystems
  -f	Find only files, not directories
  -t	Specify the number of parallel threads, default is 1
  -d	Specify the level of detail, default is 1
  -k	Keep going flag for file, can be more precise
  -r	Reverse the output sort by the number of types found
  -c	Output in columns if possible, useful if there's many types
  -C	Output in CSV format, separator is ','
  -S	Enable CSV and specify the separator
  -h	Display this help and exit
  -V	Display script's version

```

### lsfiletype

Lsfiletype complète lsfile en permettant lister les fichiers avec un
ou plusieurs types sélectionnés. Cela permet un usage plus précis que
find pour produire une liste des fichiers correspondant à un certain
type.

Nous utiliserons find, xargs et file. La version 1.0.0 n'est pas
encore prête pour le moment.

#### Usage

```
Usage: lsfiletype [OPTIONS]... [FILE]...
Generate a list of file types presents in given paths and their sum. Version 1.

  -s	Select file types, comma separated values
  -x	Don't descend directories on other filesystems
  -t	Specify the number of parallel threads, default is 1
  -h	Display this help and exit
  -V	Display script's version
```

## Tests

Nous avons un petit environnement de test qui devra sans doute
s'étoffer afin de tester le fonctionnement attendu des fonctionnalités
des utilitaires du projet.

Au lancement il créer une structure provisoire pour les tests qui sera
susceptible d'évoluer.
