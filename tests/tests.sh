#!/bin/sh

# Voici comment implémenter une suite de tests pour prévenir les bugs

# On utilise la locale standard C pour garantir l'ordre de tri des caractères,
# en effet, unicode et ascii sont triés différemments par sort notemment.
export LC_ALL=C
export LC_COLLATE=C
FILES_DIRS="
	files/empty-dir
	files/type-test
	files/path-test
"
NL="
"

LSFILE_VERSION="1.0.0"

PATH="$PATH:.."

# On a besoin d'un générateur de séquences, le langage bc là pour nous
# en bash, ksh, zsh, on utilise la forme '{x..y}'.
_seq() (
	first=$1 incr=$2 last=$3
	cat <<-eof | bc
		for (i = $first; i <= $last; i+=$incr) i
	eof
)

# On a besoin d'un contrôle de somme pour vérifier que la sortie du programme
# est celle attendue, cette construction permet de supporter simtanément
# plusieurs systèmes.
_sha256() {
	(sha256sum 2>/dev/null | sed 's/ .*-.*$//') || sha256
}

# Voilà la piece maîtraisse, une simple comparaison de string. Avec un poil
# d'utilité en affichant le nom du test et si il est réussi ou pas.
assert() (
	test_name="$1"
	need="$(printf %s "${2:-0}" | _sha256)"
	got="$(printf %s "${3:-1}" | _sha256)"
	(
		if test "$got" = "$need"; then
			echo "$test_name: PASSED"
		else
			echo "$test_name: FAILED"
		fi
	) | awk -F: '{ printf("%30s:%s\n", $1, $2) }'
)

# Pour tester si le sort qu'on a est bon, prenons la séquence ascii entre 33 et
# 126
# shellcheck disable=SC2059
ascii_seq="$(
	for i in $(_seq 32 1 126); do
		printf "\\$(printf %03o "$i")\\n"
	done
)"


prepare() (
	set -ef # -f no globbing
	#shellcheck disable=SC2086
	mkdir -p $FILES_DIRS
	IFS=$NL
	for char in $ascii_seq; do
		touch "files/path-test/${char}"
	done
	echo "ASCII file" > "files/type-test/ascii"
	echo "ASCII file" > "files/type-test/ascii "
	echo "#!/bin/sh$NL"> "files/type-test/script"

	echo >&2 "Fileset generated"
)

prepare

if ! cd files; then
	echo >&2 "Please run from the 'tests' directory"
	exit 2
fi

# Voilà, ces trois petites fonctions suffisent pour faire de vrais tests et
# vont prévenir beaucoup de soucis. On lance donc nos tests:
assert 'locale sort test' \
	"$ascii_seq" \
	"$(printf %s\\n "$ascii_seq" | sort)"
# Pour être clair, on compare une séquence ASCII avec cette même séquence triée
# par sort, assert génère les sommes sha256 de ce qui est requis et de
# ce qui est obtenu, via l'argument 2 et 3 et nous affiche le résultat préfixé
# du nom choisi avec l'argument 1.

# Les tests se lancent dans le répertoire courant comportant ce fichier même et
# un répertoire de fichiers fictifs pour les tests.
# On utilise cat et un heredoc ici pour déclarer ce qui est attendu, attention
# un seul caractère différent et ça échoue.
echo "Testing lsfile: $LSFILE_VERSION"
assert lsfile "$(cat <<-eof
     1 POSIX shell script
     2 ASCII text
     4 directory
    93 empty
eof
)" "$(lsfile)"

# Testons lsfile -r
assert 'lsfile -r' "$(cat <<-eof
    93 empty
     4 directory
     2 ASCII text
     1 POSIX shell script
eof
)" "$(lsfile -r)"

assert 'lsfile -C' "$(cat <<-eof
1,"POSIX shell script"
2,"ASCII text"
4,"directory"
93,"empty"
eof
)" "$(lsfile -C)"

assert 'lsfile -S--' "$(cat <<-eof
1--"POSIX shell script"
2--"ASCII text"
4--"directory"
93--"empty"
eof
)" "$(lsfile -S--)"

