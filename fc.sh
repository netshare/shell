#shellcheck disable=SC2048,SC2086
lc() {
	# Interactice directory chooser
	#  usage: lc [dir...]
	#
	# Installation:
	#  - Download it and source it in your shell environment
	n=1

	# Getting a depth 1 list of dirs from arguments, defaults to '.'
	list="$(
		find "${@:-.}" -maxdepth 1 \( ! -name '.*' \) -type d |
			while read -r dir; do
				printf '%3d: %s\n' "$n" "$dir"
				n=$((n + 1))
			done
	)"

	# Format the output, fallback on cat if there's no column
	printf %s\\n "${list}" | (column 2>/dev/null | cat -)

	# Return if there's no subdirectories found
	if test -z "$list"; then
		echo 1>&2 "no subdir"
		return 1
	fi

	# Interactive selection, can also match part of the dir name
	while :; do
		printf %s "selection: "
		read -r answer
		d=$(printf %s "$list" | grep "^.*$answer" | head -n1 | cut -d: -f2-)
		d=${d#' '} # Cutting the leading space
		if test -d "$d"; then
			cd "$d" || return 2
			return
		else
			echo 2>&1 "wrong selection"
		fi
	done

	unset n list answer
}
